package com.example.projecttwo.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projecttwo.Models.Item;
import com.example.projecttwo.R;

import java.util.ArrayList;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListItemViewHolder> {

    ArrayList<Item> items;
    OnListAdapterClickListener listener;

    public interface OnListAdapterClickListener{
        void onFavoriteClicked(Item item);
    }

    public ListAdapter(ArrayList<Item> items, OnListAdapterClickListener listener) {
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ListItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item, parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ListItemViewHolder holder, final int position) {

        holder.item_image.setImageResource(items.get(position).getImage());
        holder.item_category.setText(items.get(position).getCategory());
        holder.item_subcategory.setText(items.get(position).getSubcategory());
        if (items.get(position).isFavorite()){
            holder.item_favorite.setImageResource(R.drawable.ic_favorite);
        } else {
            holder.item_favorite.setImageResource(R.drawable.icnot_favorite);
        }
        holder.item_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeItemStatus(position);
                listener.onFavoriteClicked(items.get(position));

            }
        });

    }

    private void changeItemStatus(int position) {
        items.get(position).changeFavoriteStatus();
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ListItemViewHolder extends RecyclerView.ViewHolder{

        private AppCompatImageView item_image;
        private AppCompatTextView item_category;
        private AppCompatTextView item_subcategory;
        private AppCompatImageView item_favorite;

        public ListItemViewHolder(@NonNull View itemView) {
            super(itemView);

            item_image = itemView.findViewById(R.id.item_image);
            item_category = itemView.findViewById(R.id.item_category);
            item_subcategory = itemView.findViewById(R.id.item_subcategory);
            item_favorite = itemView.findViewById(R.id.item_favorite);
        }
    }
}
