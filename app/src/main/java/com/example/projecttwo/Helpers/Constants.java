package com.example.projecttwo.Helpers;

public class Constants {

    public static final boolean MALE = true;
    public static final boolean FEMALE = false;

    public static final boolean FAVORITE = true;
    public static final boolean NOT_FAVORITE = false;

    public static final String LIST_TYPE = "list type";
    public static final String WHOLE_LIST = "whole list";
    public static final String FAVORITE_LIST = "favorite list";

    public static final String USER = "user";

    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";

    public static final String CARS = "Car";
    public static final String SPORTS = "Sports";
    public static final String COUNTRIES = "Country";

    public static final String SUBCATEGORY = "subcategory";

    public static final String BMW = "bmw";
    public static final String MERCEDES = "mercedes";
    public static final String TOYOTA = "toyota";

    public static final String FOOTBALL = "football";
    public static final String BASKETBALL = "basketball";
    public static final String TENNIS = "tennis";

    public static final String EUROPE = "europe";
    public static final String ASIA = "asia";
    public static final String AFRICA = "africa";

}
