package com.example.projecttwo.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class User  implements Parcelable {
    private String name;
    private String surname;
    private String username;
    private String password;
    private String birthday;
    private boolean gender;
    private boolean notifications;

    public User(String name, String surname, String username, String password, String birthday, boolean gender, boolean notifications) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.birthday = birthday;
        this.gender = gender;
        this.notifications = notifications;
    }

    protected User(Parcel in) {
        name = in.readString();
        surname = in.readString();
        username = in.readString();
        password = in.readString();
        birthday = in.readString();
        gender = in.readByte() != 0;
        notifications = in.readByte() != 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBirthday() {
        return birthday;
    }

    public boolean getGender() {
        return gender;
    }

    public boolean hasNotifications() {
        return notifications;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(surname);
        dest.writeString(username);
        dest.writeString(password);
        dest.writeString(birthday);
        dest.writeByte((byte) (gender ? 1 : 0));
        dest.writeByte((byte) (notifications ? 1 : 0));
    }
}
