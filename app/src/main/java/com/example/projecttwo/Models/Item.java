package com.example.projecttwo.Models;

public class Item {

    private int ID;
    private String owner_username;
    private String category;
    private String subcategory;
    private int image;
    private boolean favorite;

    public Item(int ID, String owner_username, String category, String subcategory, int image, boolean favorite) {
        this.ID = ID;
        this.owner_username = owner_username;
        this.category = category;
        this.subcategory = subcategory;
        this.image = image;
        this.favorite = favorite;
    }

    public Item(String owner_username, String category, String subcategory, int image, boolean favorite) {
        ID = -1;
        this.owner_username = owner_username;
        this.category = category;
        this.subcategory = subcategory;
        this.image = image;
        this.favorite = favorite;
    }

    public int getID() {
        return ID;
    }

    public String getOwner_username() {
        return owner_username;
    }

    public String getCategory() {
        return category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public int getImage() {
        return image;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void changeFavoriteStatus() {
        favorite = !favorite;
    }
}
