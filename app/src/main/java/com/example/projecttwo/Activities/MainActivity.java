package com.example.projecttwo.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.example.projecttwo.Databases.ProjectDatabase;
import com.example.projecttwo.Databases.UserDBModel;
import com.example.projecttwo.Fragments.LogInFragment;
import com.example.projecttwo.Helpers.Constants;
import com.example.projecttwo.Models.User;
import com.example.projecttwo.R;

public class MainActivity extends AppCompatActivity implements LogInFragment.LoginFragmentListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = getSharedPreferences("checkbox", MODE_PRIVATE);
        boolean checkBoxStatus = sharedPreferences.getBoolean("remember", false);
        if (checkBoxStatus){
            SharedPreferences sharedPreferences1 = getSharedPreferences("logIn", MODE_PRIVATE);
            String username = sharedPreferences1.getString(Constants.USERNAME, "");
            assert username != null;
            if (!username.equals("")){
                logInWithUser(username);
            }
        }
    }

    private void logInWithUser(String username) {
        ProjectDatabase projectDatabase = new ProjectDatabase(this);
        SQLiteDatabase database = projectDatabase.getReadableDatabase();
        Cursor cursor = projectDatabase.checkUsername(database, username);
        if (cursor.moveToFirst()){
            String name =  cursor.getString(cursor.getColumnIndex(UserDBModel.NAME));
            String surname = cursor.getString(cursor.getColumnIndex(UserDBModel.SURNAME));

            String password = cursor.getString(cursor.getColumnIndex(UserDBModel.PASSWORD));
            String birthday = cursor.getString(cursor.getColumnIndex(UserDBModel.BIRTHDAY));
            boolean gender = (cursor.getInt(cursor.getColumnIndex(UserDBModel.GENDER)))==1 ? Constants.MALE : Constants.FEMALE;
            boolean notification = (cursor.getInt(cursor.getColumnIndex(UserDBModel.NOTIFICATIONS))) == 1;

            User user = new User(name, surname, username, password, birthday, gender, notification);
            startActivityLoggedIn(user);
        }


    }

    private void startActivityLoggedIn(User user) {
        Intent intent = new Intent(this, LoggedInActivity.class);
        intent.putExtra(Constants.USER, user);
        startActivity(intent);
    }

    @Override
    public void logIn(User user) {

        SharedPreferences sharedPreferences = getSharedPreferences( "logIn", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.USERNAME, user.getUsername());
        editor.putString(Constants.PASSWORD, user.getUsername());
        editor.apply();

        startActivityLoggedIn(user);

    }

    @Override
    public void checkedRemember(Boolean checkStatus) {
        SharedPreferences sharedPreferences = getSharedPreferences("checkbox", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("remember", checkStatus);
        editor.apply();
    }
}