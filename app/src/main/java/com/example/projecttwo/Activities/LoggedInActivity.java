package com.example.projecttwo.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.example.projecttwo.Databases.ProjectDatabase;
import com.example.projecttwo.Fragments.ListFragment;
import com.example.projecttwo.Helpers.Constants;
import com.example.projecttwo.Models.Item;
import com.example.projecttwo.Models.User;
import com.example.projecttwo.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

public class LoggedInActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        BottomNavigationView.OnNavigationItemSelectedListener,
        ListFragment.OnListFragmentListener {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private BottomNavigationView bottomNavigationView;
    private AppCompatTextView headerUsername;
    private AppCompatTextView headerNameSurname;
    private String LIST_TYPE = Constants.WHOLE_LIST;
    private String SUBCATEGORY_TYPE = Constants.FOOTBALL;

    private SharedPreferences sharedPreferences ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signed_up);
        Intent intent = getIntent();

        if (intent.hasExtra(Constants.USER) && intent.getParcelableExtra(Constants.USER) != null){
            User user = intent.getParcelableExtra(Constants.USER);
            setAll(user);
        }

        sharedPreferences = getSharedPreferences("MySharedPref", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.LIST_TYPE, Constants.WHOLE_LIST);
        editor.apply();

        getSharedPreferenceSubCategory();

        ListFragment listFragment = new ListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.LIST_TYPE, LIST_TYPE);
        bundle.putString(Constants.SUBCATEGORY, SUBCATEGORY_TYPE);
        listFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction().replace(R.id.container, listFragment,null)
                .commit();

//        if (intent.hasExtra(Constants.USER) && intent.getParcelableExtra(Constants.USER) != null){
//            User user = intent.getParcelableExtra(Constants.USER);
//            setAll(user);
//        }

//        putSharedPreferenceCategory();

    }

    private void getSharedPreferenceSubCategory() {

        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SUBCATEGORY, MODE_PRIVATE);
        SUBCATEGORY_TYPE = sharedPreferences.getString(Constants.SUBCATEGORY, Constants.FOOTBALL);

        if (SUBCATEGORY_TYPE.equals(Constants.FOOTBALL) || SUBCATEGORY_TYPE.equals(Constants.BASKETBALL)){
            bottomNavigationView.getMenu().clear();
            bottomNavigationView.inflateMenu(R.menu.bottom_navigation_menu_sports);
            SUBCATEGORY_TYPE = Constants.FOOTBALL;
        } else if (SUBCATEGORY_TYPE.equals(Constants.BMW) || SUBCATEGORY_TYPE.equals(Constants.TOYOTA) || SUBCATEGORY_TYPE.equals(Constants.MERCEDES)){
            bottomNavigationView.getMenu().clear();
            bottomNavigationView.inflateMenu(R.menu.bottom_navigation_menu_cars);
            SUBCATEGORY_TYPE = Constants.MERCEDES;
        } else if (SUBCATEGORY_TYPE.equals(Constants.EUROPE) || SUBCATEGORY_TYPE.equals(Constants.ASIA) || SUBCATEGORY_TYPE.equals(Constants.AFRICA)){
            bottomNavigationView.getMenu().clear();
            bottomNavigationView.inflateMenu(R.menu.bottom_navigation_menu_countries);
            SUBCATEGORY_TYPE = Constants.EUROPE;
        }

    }

    private void setAll(User user) {
        Toolbar toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.my_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.open_drawer, R.string.close_drawer);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        bottomNavigationView = findViewById(R.id.my_bottom_navigation_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        navigationView = findViewById(R.id.my_navigation_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);

        headerUsername = headerView.findViewById(R.id.header_username);
        headerNameSurname = headerView.findViewById(R.id.header_name_and_surname);

        headerUsername.setText(user.getUsername());
        String nameSurname = user.getName() + " " + user.getSurname();
        headerNameSurname.setText(nameSurname);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = new MenuInflater(this);
        menuInflater.inflate(R.menu.my_options_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        SharedPreferences.Editor myEdit = sharedPreferences.edit();
        Fragment fragment;

        switch (item.getItemId()){
            case R.id.item_favorite_list:{
                myEdit.putString(Constants.LIST_TYPE, Constants.FAVORITE_LIST);
                myEdit.apply();
                LIST_TYPE = Constants.FAVORITE_LIST;

                fragment = new ListFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Constants.LIST_TYPE, LIST_TYPE);
                bundle.putString(Constants.SUBCATEGORY, SUBCATEGORY_TYPE);
                fragment.setArguments(bundle);


                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, "tag" ).commit();
                break;
            }
            case R.id.item_all:{
                myEdit.putString(Constants.LIST_TYPE, Constants.WHOLE_LIST);
                myEdit.apply();
                LIST_TYPE = Constants.WHOLE_LIST;

                fragment = new ListFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Constants.LIST_TYPE, LIST_TYPE);
                bundle.putString(Constants.SUBCATEGORY, SUBCATEGORY_TYPE);
                fragment.setArguments(bundle);

                getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, "tag" ).commit();
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Bundle bundle = new Bundle();
        Fragment fragment = new Fragment();
//        SharedPreferences sp = getSharedPreferences("MySharedPref", MODE_PRIVATE);

        switch (item.getItemId())
        {

            case  R.id.item_log_out:
                openDialogue();
                break;

            case R.id.item_cars:
                bottomNavigationView.getMenu().clear();
                bottomNavigationView.inflateMenu(R.menu.bottom_navigation_menu_cars);
                LIST_TYPE = Constants.WHOLE_LIST;
                SUBCATEGORY_TYPE = Constants.MERCEDES;

                bundle.putString(Constants.SUBCATEGORY, Constants.MERCEDES);
                fragment = new ListFragment();
                fragment.setArguments(bundle);
                break;

            case R.id.item_sports:
                bottomNavigationView.getMenu().clear();
                bottomNavigationView.inflateMenu(R.menu.bottom_navigation_menu_sports);
                LIST_TYPE = Constants.WHOLE_LIST;
                SUBCATEGORY_TYPE = Constants.FOOTBALL;

                bundle.putString(Constants.SUBCATEGORY, Constants.FOOTBALL);
                fragment = new ListFragment();
                fragment.setArguments(bundle);
                break;

            case R.id.item_countries:
                bottomNavigationView.getMenu().clear();
                bottomNavigationView.inflateMenu(R.menu.bottom_navigation_menu_countries);
                LIST_TYPE = Constants.WHOLE_LIST;
                SUBCATEGORY_TYPE = Constants.EUROPE;

                bundle.putString(Constants.SUBCATEGORY, Constants.EUROPE);
                fragment = new ListFragment();
                fragment.setArguments(bundle);
                break;

            case R.id.item_bmw:
                bundle.putString(Constants.SUBCATEGORY, Constants.BMW);
                fragment = new ListFragment();
                fragment.setArguments(bundle);
                SUBCATEGORY_TYPE = Constants.BMW;
                break;
            case R.id.item_mercedes:
                bundle.putString(Constants.SUBCATEGORY, Constants.MERCEDES);
                fragment = new ListFragment();
                fragment.setArguments(bundle);
                SUBCATEGORY_TYPE = Constants.MERCEDES;
                break;
            case R.id.item_toyota:
                bundle.putString(Constants.SUBCATEGORY, Constants.TOYOTA);
                fragment = new ListFragment();
                fragment.setArguments(bundle);
                SUBCATEGORY_TYPE = Constants.TOYOTA;
                break;
            case R.id.item_football:
                bundle.putString(Constants.SUBCATEGORY, Constants.FOOTBALL);
                fragment = new ListFragment();
                fragment.setArguments(bundle);
                SUBCATEGORY_TYPE = Constants.FOOTBALL;
                break;
            case R.id.item_basketball:
                bundle.putString(Constants.SUBCATEGORY, Constants.BASKETBALL);
                fragment = new ListFragment();
                fragment.setArguments(bundle);
                SUBCATEGORY_TYPE = Constants.BASKETBALL;
                break;
            case R.id.item_africa:
                bundle.putString(Constants.SUBCATEGORY, Constants.AFRICA);
                fragment = new ListFragment();
                fragment.setArguments(bundle);
                SUBCATEGORY_TYPE = Constants.AFRICA;
                break;
            case R.id.item_asia:
                bundle.putString(Constants.SUBCATEGORY, Constants.ASIA);
                fragment = new ListFragment();
                fragment.setArguments(bundle);
                SUBCATEGORY_TYPE = Constants.ASIA;
                break;
            case R.id.item_europe:
                bundle.putString(Constants.SUBCATEGORY, Constants.EUROPE);
                fragment = new ListFragment();
                fragment.setArguments(bundle);
                SUBCATEGORY_TYPE = Constants.EUROPE;
                break;
        }
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawers();
        }

        bundle.putString(Constants.LIST_TYPE, LIST_TYPE);
        setSharedPreference(SUBCATEGORY_TYPE);

        if (item.getItemId()!=R.id.item_log_out){
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, "tag").commit();
        }

        return true;
    }

    private void setSharedPreference(String subcategory) {
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.SUBCATEGORY, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.SUBCATEGORY, subcategory);
        editor.apply();
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawers();
        } else {

            openDialogue();

//            AlertDialog.Builder builder = new AlertDialog.Builder(this);
//            builder.setTitle("Log out")
//                    .setMessage("Log out from account?")
//                    .setIcon(R.drawable.ic_error)
//                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.cancel();
//                        }
//                    })
//                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            SharedPreferences sharedPreferences = getSharedPreferences("checkbox", MODE_PRIVATE);
//                            SharedPreferences.Editor editor = sharedPreferences.edit();
//                            editor.putBoolean("remember", false);
//                            editor.apply();
//                            setSharedPreference(Constants.FOOTBALL);
//                            finish();
//                        }
//                    })
//                    .show();
        }
    }

    private void openDialogue() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Log out")
                .setMessage("Log out from account?")
                .setIcon(R.drawable.ic_error)
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences sharedPreferences = getSharedPreferences("checkbox", MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("remember", false);
                        editor.apply();
                        setSharedPreference(Constants.FOOTBALL);
                        finish();
                    }
                })
                .show();
    }

    @Override
    public void favoriteClicked(Item item) {
        ProjectDatabase projectDatabase = new ProjectDatabase(this);
        SQLiteDatabase db = projectDatabase.getReadableDatabase();
        projectDatabase.updateItem(db , item);
    }
}