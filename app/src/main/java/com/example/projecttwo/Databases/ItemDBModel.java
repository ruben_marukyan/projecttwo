package com.example.projecttwo.Databases;

public class ItemDBModel {

    public static final String ITEM_TABLE_NAME = "item_table";
    public static final String ID = "id";
    public static final String USERNAME = UserDBModel.USERNAME;
    public static final String CATEGORY = "category";
    public static final String SUBCATEGORY = "subcategory";
    public static final String IMAGE = "IMAGE";
    public static final String FAVORITE = "favorite";
}
