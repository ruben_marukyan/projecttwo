package com.example.projecttwo.Databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.example.projecttwo.Helpers.Constants;
import com.example.projecttwo.Models.Item;
import com.example.projecttwo.Models.User;
import com.example.projecttwo.R;

import java.util.ArrayList;

public class ProjectDatabase extends SQLiteOpenHelper {

    private static final String CREATE_USER_TABLE = "CREATE TABLE " + UserDBModel.USER_TABLE_NAME
            + "( " + UserDBModel.USERNAME + " TEXT PRIMARY KEY, "
            + UserDBModel.NAME + " text, "
            + UserDBModel.SURNAME + " text, "
            + UserDBModel.PASSWORD + " text, "
            + UserDBModel.BIRTHDAY + " text, "
            + UserDBModel.GENDER + " integer, "
            + UserDBModel.NOTIFICATIONS + " integer);";

    private static final String DROP_USER_TABLE = "DROP TABLE IF EXISTS " + UserDBModel.USER_TABLE_NAME;
    private static final String DROP_ITEM_TABLE = "DROP TABLE IF EXISTS " + ItemDBModel.ITEM_TABLE_NAME;

    private static final String CREATE_ITEM_TABLE = " CREATE TABLE " + ItemDBModel.ITEM_TABLE_NAME
            + "( " + ItemDBModel.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + ItemDBModel.USERNAME + " text, "
            + ItemDBModel.CATEGORY + " text, "
            + ItemDBModel.SUBCATEGORY + " text, "
            + ItemDBModel.IMAGE + " integer, "
            + ItemDBModel.FAVORITE + " integer);";

    public ProjectDatabase(@Nullable Context context) {
//        super(context, UserDBModel.DATABASE_NAME, null, UserDBModel.DATABASE_VERSION);
        super(context, UserDBModel.DATABASE_NAME, null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER_TABLE);
        db.execSQL(CREATE_ITEM_TABLE);
        fillList(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_USER_TABLE);
        db.execSQL(DROP_ITEM_TABLE);
        onCreate(db);
    }

    public long addUser(SQLiteDatabase database, User user){
        ContentValues cv = new ContentValues();

        cv.put(UserDBModel.USERNAME, user.getUsername());
        cv.put(UserDBModel.NAME, user.getName());
        cv.put(UserDBModel.SURNAME, user.getSurname());
        cv.put(UserDBModel.PASSWORD, user.getPassword());
        cv.put(UserDBModel.BIRTHDAY, user.getBirthday());
        cv.put(UserDBModel.GENDER, (user.getGender()) ? 1 : 0);
        cv.put(UserDBModel.NOTIFICATIONS, (user.hasNotifications()) ? 1 : 0);

        long result = database.insert(UserDBModel.USER_TABLE_NAME, null, cv);
        return result;
    }

    public long addItem(SQLiteDatabase database, Item item){
        ContentValues cv = new ContentValues();

        cv.put(ItemDBModel.USERNAME, item.getOwner_username());
        cv.put(ItemDBModel.CATEGORY,item.getCategory());
        cv.put(ItemDBModel.SUBCATEGORY,item.getSubcategory());
        cv.put(ItemDBModel.IMAGE,item.getImage());
        cv.put(ItemDBModel.FAVORITE,item.isFavorite() ? 1 : 0);


        long result = database.insert(ItemDBModel.ITEM_TABLE_NAME, null, cv);
        return result;
    }

    private void fillList(SQLiteDatabase database){
        for ( Item item : getList()){
            addItem(database, item);
        }
    }

    private ArrayList<Item> getList(){
        ArrayList<Item> items = new ArrayList<>();

        items.add(new Item( Constants.USER, Constants.CARS, Constants.BMW, R.drawable.bmw_2, true));
        items.add(new Item( Constants.USER, Constants.CARS, Constants.BMW, R.drawable.bmw_3, false));
        items.add(new Item( Constants.USER, Constants.CARS, Constants.BMW, R.drawable.bmw_4, true));
        items.add(new Item( Constants.USER, Constants.CARS, Constants.BMW, R.drawable.bmw_5, false));
        items.add(new Item( Constants.USER, Constants.CARS, Constants.BMW, R.drawable.bmw_6, false));
        items.add(new Item( Constants.USER, Constants.CARS, Constants.BMW, R.drawable.bmw_7, true));

        items.add(new Item( Constants.USER, Constants.CARS, Constants.MERCEDES, R.drawable.mercedes_e, true));
        items.add(new Item( Constants.USER, Constants.CARS, Constants.MERCEDES, R.drawable.mercedes_a, false));
        items.add(new Item( Constants.USER, Constants.CARS, Constants.MERCEDES, R.drawable.mercedes_b, true));
        items.add(new Item( Constants.USER, Constants.CARS, Constants.MERCEDES, R.drawable.mercedes_c, false));
        items.add(new Item( Constants.USER, Constants.CARS, Constants.MERCEDES, R.drawable.mercedes_cla, true));
        items.add(new Item( Constants.USER, Constants.CARS, Constants.MERCEDES, R.drawable.mercedes_cls, true));

        items.add(new Item( Constants.USER, Constants.CARS, Constants.TOYOTA, R.drawable.toyota_avalon, false));
        items.add(new Item( Constants.USER, Constants.CARS, Constants.TOYOTA, R.drawable.toyota_camry, true));
        items.add(new Item( Constants.USER, Constants.CARS, Constants.TOYOTA, R.drawable.toyota_chr, false));
        items.add(new Item( Constants.USER, Constants.CARS, Constants.TOYOTA, R.drawable.toyota_corolla, true));
        items.add(new Item( Constants.USER, Constants.CARS, Constants.TOYOTA, R.drawable.toyota_prado, true));
        items.add(new Item( Constants.USER, Constants.CARS, Constants.TOYOTA, R.drawable.toyota_rav4, true));

        items.add(new Item( Constants.USER, Constants.SPORTS, Constants.FOOTBALL, R.drawable.pic_arsenal, false));
        items.add(new Item( Constants.USER, Constants.SPORTS, Constants.FOOTBALL, R.drawable.pic_borussia_dortmund, true));
        items.add(new Item( Constants.USER, Constants.SPORTS, Constants.FOOTBALL, R.drawable.pic_inter, true));
        items.add(new Item( Constants.USER, Constants.SPORTS, Constants.FOOTBALL, R.drawable.pic_juventus, false));
        items.add(new Item( Constants.USER, Constants.SPORTS, Constants.FOOTBALL, R.drawable.pic_man_city, true));
        items.add(new Item( Constants.USER, Constants.SPORTS, Constants.FOOTBALL, R.drawable.pic_psg, true));
        items.add(new Item( Constants.USER, Constants.SPORTS, Constants.FOOTBALL, R.drawable.pic_real_madrid, true));

        items.add(new Item( Constants.USER, Constants.SPORTS, Constants.BASKETBALL, R.drawable.pic_warriors, true));
        items.add(new Item( Constants.USER, Constants.SPORTS, Constants.BASKETBALL, R.drawable.pic_boston_celtics, false));
        items.add(new Item( Constants.USER, Constants.SPORTS, Constants.BASKETBALL, R.drawable.pic_chicago_bulls, true));
        items.add(new Item( Constants.USER, Constants.SPORTS, Constants.BASKETBALL, R.drawable.pic_cleveland_cavaliers, false));
        items.add(new Item( Constants.USER, Constants.SPORTS, Constants.BASKETBALL, R.drawable.pic_la_lakers, true));
        items.add(new Item( Constants.USER, Constants.SPORTS, Constants.BASKETBALL, R.drawable.pic_totonto_raptors, true));

        items.add(new Item( Constants.USER, Constants.COUNTRIES, Constants.EUROPE, R.drawable.ic_pic_italy, true));
        items.add(new Item( Constants.USER, Constants.COUNTRIES, Constants.EUROPE, R.drawable.ic_pic_france, false));
        items.add(new Item( Constants.USER, Constants.COUNTRIES, Constants.EUROPE, R.drawable.ic_pic_sweden, true));
        items.add(new Item( Constants.USER, Constants.COUNTRIES, Constants.EUROPE, R.drawable.ic_pic_germany, false));

        items.add(new Item( Constants.USER, Constants.COUNTRIES, Constants.AFRICA, R.drawable.ic_pic_nigeria, false));
        items.add(new Item( Constants.USER, Constants.COUNTRIES, Constants.AFRICA, R.drawable.ic_pic_israel, true));
        items.add(new Item( Constants.USER, Constants.COUNTRIES, Constants.AFRICA, R.drawable.ic_pic_egypt, false));
        items.add(new Item( Constants.USER, Constants.COUNTRIES, Constants.AFRICA, R.drawable.ic_pic_algeria, false));

        items.add(new Item( Constants.USER, Constants.COUNTRIES, Constants.ASIA, R.drawable.ic_pic_bangladesh, false));
        items.add(new Item( Constants.USER, Constants.COUNTRIES, Constants.ASIA, R.drawable.ic_pic_saudi_arabia, false));
        items.add(new Item( Constants.USER, Constants.COUNTRIES, Constants.ASIA, R.drawable.ic_pic_united_arab_emirates, false));
        items.add(new Item( Constants.USER, Constants.COUNTRIES, Constants.ASIA, R.drawable.ic_pic_japan, true));
        items.add(new Item( Constants.USER, Constants.COUNTRIES, Constants.ASIA, R.drawable.ic_pic_india, true));





        return items;
    }

    public void updateItem (SQLiteDatabase database, Item item){
        ContentValues cv = new ContentValues();
        String id = Integer.toString(item.getID());

        cv.put(ItemDBModel.USERNAME, item.getOwner_username());
        cv.put(ItemDBModel.CATEGORY,item.getCategory());
        cv.put(ItemDBModel.SUBCATEGORY,item.getSubcategory());
        cv.put(ItemDBModel.IMAGE,item.getImage());
        cv.put(ItemDBModel.FAVORITE,item.isFavorite() ? 1 : 0);

        long result = database.update(ItemDBModel.ITEM_TABLE_NAME, cv, " ID = ?", new String[]{id});

    }

    public long updateUser (SQLiteDatabase database, User user){
        ContentValues cv = new ContentValues();
        String username = user.getUsername();

        cv.put(UserDBModel.USERNAME, user.getUsername());
        cv.put(UserDBModel.NAME, user.getName());
        cv.put(UserDBModel.SURNAME, user.getSurname());
        cv.put(UserDBModel.PASSWORD, user.getPassword());
        cv.put(UserDBModel.BIRTHDAY, user.getBirthday());
        cv.put(UserDBModel.GENDER, user.getGender());
        cv.put(UserDBModel.NOTIFICATIONS, user.hasNotifications());

        long result = database.update(UserDBModel.USER_TABLE_NAME, cv, " " + UserDBModel.USERNAME + "= ?", new String[]{username});

        return result;
    }

    public Cursor getCars(SQLiteDatabase database , String category){
        return database.rawQuery("SELECT * FROM " + UserDBModel.USER_TABLE_NAME
                        +" WHERE " + ItemDBModel.CATEGORY + " LIKE '" + category + "'",
                null);
    }

    public Cursor getSubcategory (SQLiteDatabase database, String subcategory){
        return database.rawQuery("SELECT * FROM " + ItemDBModel.ITEM_TABLE_NAME
                        +" WHERE " + ItemDBModel.SUBCATEGORY + " LIKE '" + subcategory + "'",
                null);
    }

    public Cursor getCategory (SQLiteDatabase database, String category){
        return database.rawQuery("SELECT * FROM " + ItemDBModel.ITEM_TABLE_NAME
                        +" WHERE " + ItemDBModel.CATEGORY + " LIKE '" + category + "'",
                null);
    }

    public Cursor checkUsername(SQLiteDatabase database, String username){
//        String[] columns = {UserDBModel.USERNAME, UserDBModel.NAME, UserDBModel.SURNAME, UserDBModel.PASSWORD, UserDBModel.BIRTHDAY,
//                UserDBModel.GENDER, UserDBModel.NOTIFICATIONS};
         return database.rawQuery("SELECT * FROM " + UserDBModel.USER_TABLE_NAME
                         +" WHERE " + UserDBModel.USERNAME + " GLOB '" + username + "'",
                null);
    }
}
