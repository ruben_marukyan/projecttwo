package com.example.projecttwo.Databases;

public class UserDBModel {

    public static final String DATABASE_NAME = "project2_db";
    public static final String USER_TABLE_NAME = "user_table";
    public static final int DATABASE_VERSION = 1;


    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String BIRTHDAY = "birthday";
    public static final String GENDER = "gender";
    public static final String NOTIFICATIONS = "notifications";

}
