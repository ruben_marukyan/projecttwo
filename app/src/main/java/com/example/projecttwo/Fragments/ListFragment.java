package com.example.projecttwo.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projecttwo.Adapters.ListAdapter;
import com.example.projecttwo.Databases.ItemDBModel;
import com.example.projecttwo.Databases.ProjectDatabase;
import com.example.projecttwo.Helpers.Constants;
import com.example.projecttwo.Models.Item;
import com.example.projecttwo.R;

import java.util.ArrayList;

public class ListFragment extends Fragment implements ListAdapter.OnListAdapterClickListener {

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
    private ArrayList<Item> items = new ArrayList<>();
    Bundle bundle;
    boolean favorite_list = false;
    private String subcategory;
    String listType;
    OnListFragmentListener callback;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentListener){
            callback = (OnListFragmentListener) context;
        }
    }

    public interface OnListFragmentListener{
        void favoriteClicked(Item item);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bundle = getArguments();

//        checkSharedPreference(bundle);


        //Set List Type
        if (bundle != null && bundle.getString(Constants.LIST_TYPE)!= null){
            listType = bundle.getString(Constants.LIST_TYPE);
        }

        //Set subcategory
        if (bundle!= null && bundle.getString(Constants.SUBCATEGORY)!= null){
            subcategory = bundle.getString(Constants.SUBCATEGORY);

            //Getting list from DB
            ProjectDatabase projectDatabase = new ProjectDatabase(getActivity());
            SQLiteDatabase db = projectDatabase.getReadableDatabase();
            Cursor cursor = projectDatabase.getSubcategory(db, subcategory);

           if (cursor.moveToFirst()){
//               Toast.makeText(getActivity(), "Cursor is " + cursor.moveToFirst(), Toast.LENGTH_SHORT).show();
               do{
                   int id = cursor.getInt(cursor.getColumnIndex(ItemDBModel.ID));
                   String owner = cursor.getString(cursor.getColumnIndex(ItemDBModel.USERNAME));
                   String category = cursor.getString(cursor.getColumnIndex(ItemDBModel.CATEGORY));
                   String sub_category = cursor.getString(cursor.getColumnIndex(ItemDBModel.SUBCATEGORY));
                   int img = cursor.getInt(cursor.getColumnIndex(ItemDBModel.IMAGE));
                   boolean favorite = cursor.getInt(cursor.getColumnIndex(ItemDBModel.FAVORITE)) == 1;

                   if(!favorite && listType.equals(Constants.FAVORITE_LIST)) continue;

                   items.add(new Item(id, owner, category, sub_category, img, favorite));

               } while (cursor.moveToNext());
               cursor.close();
           }
        }

        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(new ListAdapter(items, this));

    }

    @Override
    public void onFavoriteClicked(Item item) {
        callback.favoriteClicked(item);

    }
}
