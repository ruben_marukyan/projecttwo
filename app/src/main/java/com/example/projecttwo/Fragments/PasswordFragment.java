package com.example.projecttwo.Fragments;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;

import com.example.projecttwo.Databases.ProjectDatabase;
import com.example.projecttwo.Databases.UserDBModel;
import com.example.projecttwo.Helpers.Constants;
import com.example.projecttwo.Models.User;
import com.example.projecttwo.R;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

public class PasswordFragment extends Fragment {

    private TextInputLayout username;
    private TextInputLayout old_password;
    private TextInputLayout new_password;
    private AppCompatButton btn_check_password;

    private User user;
    private boolean userCorrect = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_password, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        username = view.findViewById(R.id.field_username_for_check);
        old_password = view.findViewById(R.id.field_old_password);
        new_password = view.findViewById(R.id.field_new_password);
        btn_check_password = view.findViewById(R.id.btn_check_password);
        listeners();
    }

    private void listeners() {
        btn_check_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!userCorrect){
                    if (!Objects.requireNonNull(username.getEditText()).getText().toString().equals("")
                            && username.getEditText().getText()!= null)
                    {
                        String usernameText = username.getEditText().getText().toString();
                        if (checkUser(usernameText)){
                            btn_check_password.setText(R.string.apply);
                        }
                    }
                }
                else {
                    if( !Objects.requireNonNull(new_password.getEditText()).getText().toString().equals("")
                            && new_password.getEditText().getText()!=null){
                        user.setPassword(new_password.getEditText().getText().toString());
                        updateUser();
                    }
                    else {
                        Toast.makeText(getActivity(), "Username can't be void.", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });
    }

    private void updateUser() {
        ProjectDatabase projectDatabase = new ProjectDatabase(getActivity());
        SQLiteDatabase database = projectDatabase.getWritableDatabase();
        long result = projectDatabase.updateUser(database , user);
        if (result==1){
            Toast.makeText(getActivity(), "Password Changed", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkUser(String usernameText) {
        ProjectDatabase projectDatabase = new ProjectDatabase(getActivity());
        SQLiteDatabase database = projectDatabase.getWritableDatabase();
        Cursor cursor = projectDatabase.checkUsername(database, usernameText);
        if (cursor.moveToFirst())
        {
            username.setError(null);
            userCorrect = true;
            String name =  cursor.getString(cursor.getColumnIndex(UserDBModel.NAME));
            String surname = cursor.getString(cursor.getColumnIndex(UserDBModel.SURNAME));

            String password = cursor.getString(cursor.getColumnIndex(UserDBModel.PASSWORD));
            String birthday = cursor.getString(cursor.getColumnIndex(UserDBModel.BIRTHDAY));
            boolean gender = (cursor.getInt(cursor.getColumnIndex(UserDBModel.GENDER)))==1 ? Constants.MALE : Constants.FEMALE;
            boolean notification = (cursor.getInt(cursor.getColumnIndex(UserDBModel.NOTIFICATIONS))) == 1;

            Objects.requireNonNull(old_password.getEditText()).setText(password);
            old_password.setVisibility(View.VISIBLE);
            new_password.setVisibility(View.VISIBLE);
            user = new User(name, surname , usernameText, password, birthday, gender, notification);
            cursor.close();
            return true;
        }

        else {
            userCorrect = false;
            username.setError("Wrong username!");
            cursor.close();
            return false;
        }
    }
}
