package com.example.projecttwo.Fragments;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.projecttwo.Databases.ProjectDatabase;
import com.example.projecttwo.Databases.UserDBModel;
import com.example.projecttwo.Helpers.Constants;
import com.example.projecttwo.Models.User;
import com.example.projecttwo.R;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

public class LogInFragment extends Fragment {

    private TextInputLayout field_username;
    private TextInputLayout field_password;
    private CheckBox rememberCheckBox;
    private AppCompatTextView forgot_pass;
    private AppCompatButton btn_main_log_in;
    private User user;

    private LoginFragmentListener loginFragmentListener;

    public interface LoginFragmentListener{
        void logIn(User user);
        void checkedRemember(Boolean checkStatus);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof LoginFragmentListener){
            loginFragmentListener = (LoginFragmentListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_log_in, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        field_username = view.findViewById(R.id.login_username);
        field_password = view.findViewById(R.id.login_password);
        btn_main_log_in = view.findViewById(R.id.btn_log_in);
        forgot_pass = view.findViewById(R.id.txt_forgot_password_clickable);
        rememberCheckBox = view.findViewById(R.id.checkbox_log_in);
        listeners();
    }

    private void listeners() {
        btn_main_log_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (validateUsername() && validatePassword()){
                    if (checkUser()){
                        checkBoxCheck();
                        loginFragmentListener.logIn(user);
                    }
                    else {
                        field_username.setError("Wrong Username");
                    }
                }
            }
        });
        forgot_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_logInFragment_to_passwordFragment);
            }
        });

    }

    private void checkBoxCheck(){
        if (rememberCheckBox.isChecked()){
            loginFragmentListener.checkedRemember(true);
        } else{
            loginFragmentListener.checkedRemember(false);
        }
    }

    private boolean checkUser() {
        String username = field_username.getEditText().getText().toString().trim();
        String password = field_password.getEditText().getText().toString().trim();

        ProjectDatabase myDatabase = new ProjectDatabase(getActivity());
        SQLiteDatabase db = myDatabase.getReadableDatabase();
        Cursor cursor = myDatabase.checkUsername(db, username);
        if (cursor.moveToFirst())
        {
            if (cursor.getString(cursor.getColumnIndex(UserDBModel.PASSWORD)).equals(password)){
                user = new User (
                        cursor.getString(cursor.getColumnIndex(UserDBModel.NAME)),
                        cursor.getString(cursor.getColumnIndex(UserDBModel.SURNAME)),
                        cursor.getString(cursor.getColumnIndex(UserDBModel.USERNAME)),
                        cursor.getString(cursor.getColumnIndex(UserDBModel.PASSWORD)),
                        cursor.getString(cursor.getColumnIndex(UserDBModel.BIRTHDAY)),
                        (cursor.getInt(cursor.getColumnIndex(UserDBModel.GENDER)))==1 ? Constants.MALE : Constants.FEMALE,
                        (cursor.getInt(cursor.getColumnIndex(UserDBModel.NOTIFICATIONS))) == 1);
                return true;
            } else{
                field_password.setError("Wrong password");
                return false;
            }
        }
        return false;
    }

    public boolean validateUsername(){
        if (Objects.requireNonNull(field_username.getEditText()).getText().toString().isEmpty() || field_username.getEditText().getText()==null){
            field_username.setError("Write your username.");
            return false;
        }
        else{
            field_username.setError(null);
            return true;
        }
    }

    public boolean validatePassword(){
        if (Objects.requireNonNull(field_password.getEditText()).getText().toString().isEmpty() || field_password.getEditText().getText()==null){
            field_password.setError("Write your password.");
            return false;
        }
        else{
            field_password.setError(null);
            return true;
        }
    }
}
