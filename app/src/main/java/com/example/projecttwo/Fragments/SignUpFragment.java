package com.example.projecttwo.Fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.example.projecttwo.Activities.LoggedInActivity;
import com.example.projecttwo.Databases.ProjectDatabase;
import com.example.projecttwo.Helpers.Constants;
import com.example.projecttwo.Models.User;
import com.example.projecttwo.R;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;

public class SignUpFragment extends Fragment  implements DatePickerDialog.OnDateSetListener {

    private TextInputLayout field_name;
    private TextInputLayout field_surname;
    private TextInputLayout field_username;
    private TextInputLayout field_password;
    private TextInputLayout field_password_confirm;

    private AppCompatImageView birthday_img;
    private AppCompatTextView txt_birthday;

    private RadioGroup genderRadioGroup;
    private AppCompatCheckBox checkBox_notifications;
    private AppCompatButton btn_add_user;


    private String name;
    private String surname;
    private String username;
    private String password;
    private String birthday = "6554554";

    private boolean gender = true;
    private boolean notifications = false;
    private boolean birthdayChosen = false;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_up,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        field_name = view.findViewById(R.id.txt_input_name);
        field_surname = view.findViewById(R.id.txt_input_surname);
        field_username = view.findViewById(R.id.txt_input_username);
        field_password = view.findViewById(R.id.txt_input_password);
        field_password_confirm = view.findViewById(R.id.txt_input_password_confirm);

        birthday_img = view.findViewById(R.id.img_birthday);
        txt_birthday = view.findViewById(R.id.txt_picked_date);


        genderRadioGroup = view.findViewById(R.id.radioGroup);

        checkBox_notifications = view.findViewById(R.id.checkbox_notifications);
        btn_add_user = view.findViewById(R.id.btn_sign_up);
        listeners();
    }

    private void listeners() {
        genderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId){
                    case R.id.radio_btn_male:
                        gender = Constants.MALE;
                        break;
                    case R.id.radio_btn_female:
                        gender = Constants.FEMALE;
                }
            }
        });

        birthday_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               createDialog();
            }
        });

        btn_add_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getActivity(), "btn_clicked", Toast.LENGTH_SHORT).show();
                if (validateName() && validateSurname()
                        && validateUsername() && validatePassword() && validateBirthday() && birthdayChosen){
                    name = field_name.getEditText().getText().toString();
                    surname = field_surname.getEditText().getText().toString();
                    username = field_username.getEditText().getText().toString().trim();
                    password = field_password.getEditText().getText().toString().trim();
                    notifications = checkBox_notifications.isChecked();
                    User user = new User(name, surname, username, password , birthday, gender, notifications);
                     if (checkUsernameInDB(user)){
                         Intent intent = new Intent(getActivity(), LoggedInActivity.class);
                         intent.putExtra(Constants.USER, user);
                         startActivity(intent);
                     }
                }
            }
        });
    }

    private void createDialog() {
        Calendar calendar =Calendar.getInstance();
        int year=calendar.get(Calendar.YEAR);
        int month=calendar.get(Calendar.MONTH);
        int day=calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog=new DatePickerDialog(requireActivity(),this ,year ,month ,day);

        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        int yearToday = calendar.get(Calendar.YEAR);
        int monthToday=calendar.get(Calendar.MONTH);
        int dayToday=calendar.get(Calendar.DAY_OF_MONTH);

        if (year > yearToday || (year == yearToday && month > monthToday)
                || (year== yearToday && month == monthToday && dayOfMonth > dayToday)){
            Toast.makeText( getActivity(), "Are you born in the future?", Toast.LENGTH_SHORT).show();
            txt_birthday.setText(R.string.wrong_date);
            txt_birthday.setTextColor(getResources().getColor(R.color.design_default_color_error));
            birthdayChosen = false;
        }
        else if (yearToday== year && monthToday== month && dayOfMonth == dayToday){
            Toast.makeText( getActivity(), "Happy Birthday!", Toast.LENGTH_SHORT).show();
            txt_birthday.setText(R.string.wrong_date);
            txt_birthday.setTextColor(getResources().getColor(R.color.design_default_color_error));
            birthdayChosen = false;
        }
        else if (year>= yearToday-5 ){
            Toast.makeText( getActivity(), "You must be older than 6!", Toast.LENGTH_SHORT).show();
            txt_birthday.setText(R.string.wrong_date);
            txt_birthday.setTextColor(getResources().getColor(R.color.design_default_color_error));
            birthdayChosen = false;
        }
        else {
            birthdayChosen = true;
            birthday = year + "/" + (month + 1) + "/" + dayOfMonth;
            txt_birthday.setTextColor(getResources().getColor(R.color.colorSecondaryAccent));
            txt_birthday.setText(birthday);
        }



    }


    private boolean validateBirthday() {


        return true;
    }

//    public void createDialog() {
//        AlertDialog.Builder builder = new DatePickerDialog(getActivity())
//    }

    public boolean validateName(){
        if (Objects.requireNonNull(field_name.getEditText()).getText().toString().isEmpty() || field_name.getEditText().getText()== null){
            field_name.setError("Can't be empty");
            return false;
        }
        else{
            field_name.setError(null);
            return true;
        }
    }

    public boolean validateSurname(){
        if (Objects.requireNonNull(field_surname.getEditText()).getText().toString().isEmpty() || field_surname.getEditText().getText()==null){
            field_surname.setError("Can't be empty!");
            return false;
        }
        else{
            field_surname.setError(null);
            return true;
        }
    }

    public boolean validateUsername(){
        if (Objects.requireNonNull(field_username.getEditText()).getText().toString().isEmpty() || field_username.getEditText().getText()==null){
            field_username.setError("Can't be empty!");
            return false;
        }
        else{
            field_username.setError(null);
            return true;
        }
    }

    private boolean checkUsernameInDB(User user) {
        ProjectDatabase myDatabase = new ProjectDatabase(getActivity());
        SQLiteDatabase db = myDatabase.getWritableDatabase();
        if(myDatabase.checkUsername(db, user.getUsername()).moveToFirst()){
            Toast.makeText(getActivity(), "Username Exists!", Toast.LENGTH_SHORT).show();
            field_username.setError("Username already exists, create new one.");
            return false;
        } else{
            myDatabase.addUser(db, user);
            field_username.setError(null);
            return true;
        }

    }

    public boolean validatePassword(){
        field_password.setError(null);
        field_password_confirm.setError(null);
        if (Objects.requireNonNull(field_password.getEditText()).getText().toString().isEmpty() || field_password.getEditText().getText() == null){
            field_password.setError("Can't be empty!");
            return false;
        }
        else if(Objects.requireNonNull(field_password_confirm.getEditText()).getText().toString().isEmpty() || field_password_confirm.getEditText().getText() == null){
            field_password_confirm.setError("Please confirm password!");
            return false;
        }
        else if (!field_password.getEditText().getText().toString().trim()
                .equals(field_password_confirm.getEditText().getText().toString().trim())){

            field_password_confirm.setError("Passwords didn't match!");
            return false;
        }
        else {
            field_password.setError(null);
            field_password_confirm.setError(null);
            return true;
        }
    }



}
