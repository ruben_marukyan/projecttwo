package com.example.projecttwo.Fragments;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.projecttwo.R;

public class MainFragment extends Fragment implements View.OnClickListener {

    private AppCompatTextView titleName;
    private AppCompatButton btn_main_sign_up;
    private AppCompatButton btn_main_log_in;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        titleName = view.findViewById(R.id.txt_app_name);
        Typeface typeface = getResources().getFont(R.font.fire_brathers);
        titleName.setTypeface(typeface);
        btn_main_log_in = view.findViewById(R.id.btn_main_log_in);
        btn_main_log_in.setOnClickListener(this);
        btn_main_sign_up = view.findViewById(R.id.btn_main_sign_up);
        btn_main_sign_up.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_main_log_in:{
                Navigation.findNavController(btn_main_log_in).navigate(R.id.action_mainFragment_to_logInFragment);
                break;
            }
            case R.id.btn_main_sign_up:{
                Navigation.findNavController(btn_main_sign_up).navigate(R.id.action_mainFragment_to_signUpFragment);
                break;
            }
        }
    }
}
